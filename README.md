# Paycoiner API Client

## Supported Futures

Endpoint/Webhook | Supported?
--- | --- 
Create Payouts | ✅
Payouts Status | ✅
Deposit Addresses | ✅
Deposit Received | ✅
Create Payment Invoice | ✅
Get Payment Invoice | ✅
Instant Payment Notifications | ✅
Address Validator | ✅


## Installation

#### Maven

```xml
<dependency>
  <groupId>com.paycoiner</groupId>
  <artifactId>client</artifactId>
  <version>1.0.2</version>
</dependency>
```

#### Gradle

```groovy
implementation 'com.paycoiner:client:1.0.2'
```

## Environment variables

```
# paycoiner infrastucture. Available: merchant or sandbox. default sandbox
PAYCOINER__INFRA=sandbox
# your customer id received after project creation
PAYCOINER__CUSTOMER_ID=
# webhook key received after project creation
PAYCOINER__WEBHOOK_KEY=
# endpoint key received after project creation
PAYCOINER__ENDPOINT_KEY=
# private key to sign payout request.
PAYCOINER__PRIVATE_KEY_PATH=
```


## Webhooks

implemented:
* `DepositsHandler` - to handle deposits status
* `InvoiceHandler` - to handle invoice status
* `PayoutsHandler` - to handle invoice status

example usage for symfony project:
```java
package com.example;

import com.paycoiner.client.domains.transfers.Invoice;
import com.paycoiner.client.handlers.InvoicesHandler;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/handlers/invoices")
public class InvoicesHandleController {
    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public void invoiceHandle(HttpServletRequest req) {
        Invoice invoice = (new InvoicesHandler()).handle(req);
        // some actions with invoice
    }
}

```

# Endpoint calls

All calls are in `com.paycoiner.client.PaycoinerClient`.

example invoice creation:

```java
import com.paycoiner.client.domains.transfers.Invoice;
import com.paycoiner.client.domains.requests.CreateInvoice;

import com.paycoiner.client.PaycoinerClient;

public class Controller {
    public void createInvoice() {
        (new PaycoinerClient()).createInvoice(new CreateInvoice(
            "order-id",
            "BTC", // base currency
            "BTC", // quote currency
            "0.002", // amount 
            "email@example.com" // customer email (not-required)
        ));
    }
}
```
