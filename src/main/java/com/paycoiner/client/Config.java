package com.paycoiner.client;

import com.paycoiner.client.enums.Infra;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Config {
    private static Config config;

    private Infra infra;
    private String customerId;
    private String webhookKey;
    private String endpointKey;
    private String privateKeyPath;
    private String rawPrivateKey;

    public String getCustomerId() {
        if (customerId == null) {
            customerId = System.getenv(Constant.ENV_CUSTOMER_ID);
        }

        return customerId;
    }

    public String getWebhookKey() {
        if (webhookKey == null) {
            webhookKey = System.getenv(Constant.ENV_WEBHOOK_KEY);
        }

        return webhookKey;
    }

    public String getEndpointKey() {
        if (endpointKey == null) {
            endpointKey = System.getenv(Constant.ENV_ENDPOINT_KEY);
        }

        return endpointKey;
    }

    public Infra getInfra() {
        if (infra != null) {
            return infra;
        }

        String rawInfra = System.getenv(Constant.ENV_INFRA);
        if (rawInfra == null) {
            return infra = getDefaultInfra();
        }

        try {
            return infra = Infra.valueOf(rawInfra.toUpperCase());
        } catch (IllegalArgumentException e) {
            return infra = getDefaultInfra();
        }
    }

    public String getLowerCaseInfra() {
        return getInfra().name().toLowerCase();
    }

    private Infra getDefaultInfra() {
        return Infra.SANDBOX;
    }

    public String getDepositAddressesUrl() {
        return "https://deposit-addresses." + getLowerCaseInfra() + ".paycoiner.com/api/v1";
    }

    public String getPaymentsUrl() {
        return "https://payments." + getLowerCaseInfra() + ".paycoiner.com/api/v1";
    }

    public String getPayoutsUrl() {
        return "https://payouts." + getLowerCaseInfra() + ".paycoiner.com/api/v2";
    }

    public String getAddressValidatorUrl() {
        return "https://address-validator." + getLowerCaseInfra() + ".paycoiner.com/api/v1";
    }

    public String getPrivateKeyPath() {
        if (privateKeyPath == null) {
            privateKeyPath = System.getenv(Constant.ENV_PRIVATE_KEY_PATH);
        }

        return privateKeyPath;
    }

    public String getRawPrivateKey() {
        if (rawPrivateKey != null) {
            return rawPrivateKey;
        }

        String path = this.getPrivateKeyPath();
        if (path == null) {
            return null;
        }

        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(path));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }

        String line;
        StringBuilder builder = new StringBuilder();
        while (true) {
            try {
                line = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
                line = null;
            }
            if (line == null) {
                break;
            }
            builder.append(line);
        }

        return rawPrivateKey = builder.toString();
    }

    public static Config getInstance() {
        if (config == null) {
            config = new Config();
        }
        return config;
    }
}
