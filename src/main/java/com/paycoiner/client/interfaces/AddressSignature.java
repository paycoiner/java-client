package com.paycoiner.client.interfaces;

public interface AddressSignature {
    String getSignature();

    String getCurrency();
    String getAddress();
    String getPaymentTag();
}
