package com.paycoiner.client;

public class Constant {
    public static final String HEADER_HMAC = "HMAC";

    public static final String ENV_INFRA = "PAYCOINER__INFRA";
    public static final String ENV_CUSTOMER_ID = "PAYCOINER__CUSTOMER_ID";
    public static final String ENV_WEBHOOK_KEY = "PAYCOINER__WEBHOOK_KEY";
    public static final String ENV_ENDPOINT_KEY = "PAYCOINER__ENDPOINT_KEY";
    public static final String ENV_PRIVATE_KEY_PATH = "PAYCOINER__PRIVATE_KEY_PATH";
}
