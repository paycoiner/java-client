package com.paycoiner.client.enums;

public enum InvoiceStatus {
    /** pending time above 24h, paid but 0 confirmations*/
    UNSUCCESSFUL,

    /** transaction count (no pending transactions while … ) = 0 and time for payment expired */
    EXPIRED,

    /** newly generated payment request (invoice), user hasn't made any transfer yet and there is still time for payment) */
    NEW,

    /** transaction count greater than 0 and not all transactions are completed */
    PENDING,

    /** amount received greater than amount stated, based on confirmed transactions */
    OVERPAID,

    /** amount received less than amount stated, based on confirmed transactions */
    UNDERPAID,

    /** amount received = amount stated, based on confirmed transactions */
    SUCCESSFUL
}
