package com.paycoiner.client.enums;

public enum PayoutStatus {
    SECURITY_ALERT,
    ERROR_TO_VERIFY,
    PAUSED,
    NEW,
    PROCESSING,
    DURING_PAYOUT,
    PAID_OUT,
    DELAYED,
    UNKNOWN,
    NOT_EXCHANGED
}
