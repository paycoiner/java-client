package com.paycoiner.client.services;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class HmacService {
    private static HmacService instance;

    private static final String ALGO = "HmacSHA256";

    public String generate(String payload, String key) {
        if (payload == null || key == null) {
            return null;
        }

        Mac algo;
        try {
            algo = Mac.getInstance(ALGO);
            algo.init(new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), ALGO));
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            e.printStackTrace();
            return null;
        }

        return (new HexBinaryAdapter()).marshal(algo.doFinal(payload.getBytes(StandardCharsets.UTF_8))).toLowerCase();
    }

    public boolean validate(String hash, String key, String payload) {
        String expectedHash = this.generate(payload, key);
        if (hash == null || expectedHash == null || expectedHash.isEmpty()) {
            return false;
        }

        return expectedHash.equals(hash);
    }

    public static HmacService getInstance() {
        if (instance == null) {
            instance = new HmacService();
        }

        return instance;
    }
}
