package com.paycoiner.client.services;

import org.apache.commons.io.IOUtils;

import javax.servlet.ServletRequest;
import java.io.IOException;

public class RequestService {
    public String getPayload(ServletRequest request) {
        try {
            return IOUtils.toString(request.getReader());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
