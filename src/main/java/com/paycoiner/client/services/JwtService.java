package com.paycoiner.client.services;

import com.google.gson.Gson;
import com.paycoiner.client.Config;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

public class JwtService {
    private static JwtService instance;

    private PKCS8EncodedKeySpec getKeySpecFromRawPrivateKey(String rawKey) {
        return new PKCS8EncodedKeySpec(Base64.getDecoder().decode(
                rawKey.replaceAll("\\s+", "")
                        .replaceAll("-----(BEGIN|END)(RSA)?PRIVATEKEY-----", "")
        ));
    }

    private X509EncodedKeySpec getKeySpecFromRawPublicKey(String rawKey) {
        return new X509EncodedKeySpec(Base64.getDecoder().decode(
                rawKey.replaceAll("\\s+", "")
                        .replaceAll("-----(BEGIN|END)(RSA)?PUBLICKEY-----", "")
        ));
    }

    public PrivateKey getPrivateKey(String rawPrivateKey) {
        try {
            return KeyFactory.getInstance("RSA").generatePrivate(this.getKeySpecFromRawPrivateKey(rawPrivateKey));
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
            return null;
        }
    }

    public PublicKey getPublicKey(String rawPubKey)  {
        try {
            return KeyFactory.getInstance("RSA").generatePublic(this.getKeySpecFromRawPublicKey(rawPubKey));
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getJwt(Object obj) {
        Gson gson = new Gson();
        @SuppressWarnings("rawtypes") HashMap data = gson.fromJson(gson.toJson(obj), HashMap.class);

        Claims claims = Jwts.claims();
        for (Object el : data.entrySet()) {
            @SuppressWarnings("rawtypes") Map.Entry entry = (Map.Entry) el;
            claims.put(entry.getKey().toString(), entry.getValue().toString());
        }

        return Jwts.builder()
                .setClaims(claims)
                .signWith(this.getPrivateKey(Config.getInstance().getRawPrivateKey()))
                .compact();
    }

    public static JwtService getInstance() {
        if (instance == null) {
            instance = new JwtService();
        }

        return instance;
    }
}
