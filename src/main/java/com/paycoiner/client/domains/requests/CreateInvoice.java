package com.paycoiner.client.domains.requests;

import com.paycoiner.client.Config;

public class CreateInvoice {
    /** Your customer ID (UUID) */
    public String customerId = Config.getInstance().getCustomerId();

    /** Payment identifier. It must be unique. We reject requests with replicated orderId. */
    public String orderId;

    /** Currency ticker. uppercase, example: BTC */
    public String baseCurrency;

    /** Cryptocurrency ticker, user will transfer money in this currency. example: BTC */
    public String quoteCurrency;

    /** The total amount of the payment (in baseCurrency). */
    public String amount;

    /** Buyer's e-mail. Could be null */
    public String email;

    /** unique nonce to make request more private */
    public Long nonce = System.currentTimeMillis();

    public CreateInvoice(String orderId, String baseCurrency, String quoteCurrency, String amount) {
        this(orderId, baseCurrency, quoteCurrency, amount, null);
    }

    public CreateInvoice(String orderId, String baseCurrency, String quoteCurrency, String amount, String email) {
        this.orderId = orderId;
        this.baseCurrency = baseCurrency;
        this.quoteCurrency = quoteCurrency;
        this.amount = amount;
        this.email = email;
    }
}
