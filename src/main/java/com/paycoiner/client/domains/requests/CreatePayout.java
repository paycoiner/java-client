package com.paycoiner.client.domains.requests;

import com.paycoiner.client.Config;

public class CreatePayout {
    /** Your customer ID (UUID) */
    public String customerId = Config.getInstance().getCustomerId();

    /** uuid from customer system */
    public String orderId;

    /** cryptocurrency. uppercase, example: BTC */
    public String currency;

    /** to which address send {amount} */
    public String address;

    /** the exact amount to send to {address} */
    public String amount;

    /** unique nonce to make request more private */
    public Long nonce = System.currentTimeMillis();

    public CreatePayout(String orderId, String currency, String address, String amount) {
        this.orderId = orderId;
        this.currency = currency;
        this.address = address;
        this.amount = amount;
    }
}
