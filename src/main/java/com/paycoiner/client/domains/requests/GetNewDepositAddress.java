package com.paycoiner.client.domains.requests;

import com.paycoiner.client.Config;

public class GetNewDepositAddress {
    /** customer uuid */
    public String customerId = Config.getInstance().getCustomerId();

    /** uuid from customer system */
    public String orderId;

    /** uppercase, example: BTC */
    public String currency;

    /** unique nonce to make request more private */
    public Long nonce = System.currentTimeMillis();

    public GetNewDepositAddress(String orderId, String currency) {
        this.orderId = orderId;
        this.currency = currency.toUpperCase();
    }
}
