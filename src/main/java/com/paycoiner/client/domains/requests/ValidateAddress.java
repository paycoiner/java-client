package com.paycoiner.client.domains.requests;

public class ValidateAddress {
    /** cryptocurrency. uppercase, example: BTC */
    public String currency;

    /** crypto address */
    public String address;

    public ValidateAddress(String currency, String address) {
        this.currency = currency;
        this.address = address;
    }
}
