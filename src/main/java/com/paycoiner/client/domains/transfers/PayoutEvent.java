package com.paycoiner.client.domains.transfers;

public class PayoutEvent {
    /** Paycoiner unique uuid */
    public String payoutId;

    /** customer uuid */
    public String customerId;

    /** uuid from customer system */
    public String orderId;

    public String fee;

    public String txId;
}
