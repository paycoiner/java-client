package com.paycoiner.client.domains.transfers;

public class Deposit {
    public String depositId;
    public String customerId;
    public String txId;
    public String address;
    public String amount;
    public String currency;
    public Integer blockHeight = null;
    public Integer confirmations = 0;
    public boolean finalConfirmed = false;
    public boolean rejected = false;
}
