package com.paycoiner.client.domains.transfers;

import com.paycoiner.client.enums.PayoutStatus;

public class Payout {
    public String payoutId;
    public String orderId;
    public String customerId;
    public String currency;
    public String amount;
    public String address;
    public String paymentTag;
    public PayoutStatus status;
}
