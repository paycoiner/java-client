package com.paycoiner.client.handlers;

import com.google.gson.Gson;
import com.paycoiner.client.domains.transfers.Invoice;
import com.paycoiner.client.exceptions.InvalidSignature;
import com.paycoiner.client.validators.AddressSignatureValidator;

public class InvoicesHandler extends HmacHandler<Invoice> {
    public InvoicesHandler() {
        super();
    }

    public InvoicesHandler(String webhookKey) {
        super(webhookKey);
    }

    @Override
    protected void additionalValidation(Invoice invoice) throws InvalidSignature {
        super.additionalValidation(invoice);
        if (!(new AddressSignatureValidator()).validate(invoice)) {
            throw new InvalidSignature();
        }
    }

    @Override
    protected Invoice castPayload(String payload) {
        return (new Gson()).fromJson(payload, Invoice.class);
    }
}
