package com.paycoiner.client.handlers;

import com.paycoiner.client.Config;
import com.paycoiner.client.Constant;
import com.paycoiner.client.exceptions.InvalidHash;
import com.paycoiner.client.exceptions.InvalidSignature;
import com.paycoiner.client.services.HmacService;
import com.paycoiner.client.services.RequestService;

import javax.servlet.http.HttpServletRequest;

abstract public class HmacHandler<T> {
    private final String webhookKey;
    private final HmacService hmacService = new HmacService();
    private final RequestService requestService = new RequestService();

    public HmacHandler() {
        this(Config.getInstance().getWebhookKey());
    }

    public HmacHandler(String webhookKey) {
        this.webhookKey = webhookKey;
    }

    abstract protected T castPayload(String payload);

    protected void additionalValidation(T obj) throws InvalidSignature {

    }

    public T handle(String payload, String hmac) throws InvalidHash, InvalidSignature {
        if (!this.hmacService.validate(hmac, this.webhookKey, payload)) {
            throw new InvalidHash();
        }
        T obj = this.castPayload(payload);
        this.additionalValidation(obj);

        return obj;
    }

    public T handle(HttpServletRequest request) throws InvalidHash, InvalidSignature {
        return handle(this.requestService.getPayload(request), request.getHeader(Constant.HEADER_HMAC));
    }
}
