package com.paycoiner.client.handlers;

import com.google.gson.Gson;
import com.paycoiner.client.domains.transfers.PayoutEvent;

public class PayoutsHandler extends HmacHandler<PayoutEvent> {
    public PayoutsHandler() {
        super();
    }

    public PayoutsHandler(String webhookKey) {
        super(webhookKey);
    }

    @Override
    protected PayoutEvent castPayload(String payload) {
        return (new Gson()).fromJson(payload, PayoutEvent.class);
    }
}
