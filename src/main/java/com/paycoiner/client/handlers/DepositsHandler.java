package com.paycoiner.client.handlers;

import com.google.gson.Gson;
import com.paycoiner.client.domains.transfers.Deposit;

public class DepositsHandler extends HmacHandler<Deposit> {
    public DepositsHandler() {
        super();
    }

    public DepositsHandler(String webhookKey) {
        super(webhookKey);
    }

    @Override
    protected Deposit castPayload(String payload) {
        return (new Gson()).fromJson(payload, Deposit.class);
    }
}
