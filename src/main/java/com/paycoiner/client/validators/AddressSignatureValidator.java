package com.paycoiner.client.validators;

import com.paycoiner.client.Config;
import com.paycoiner.client.exceptions.InvalidSignature;
import com.paycoiner.client.interfaces.AddressSignature;
import com.paycoiner.client.services.JwtService;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.Base64;

public class AddressSignatureValidator {
    private static AddressSignatureValidator instance;

    public void check(AddressSignature addressSignature) throws InvalidSignature {
        if (this.validate(addressSignature)) {
            return;
        }

        throw new InvalidSignature();
    }

    public boolean validate(AddressSignature addressSignature) {
        Signature signature;
        try {
            signature = Signature.getInstance("SHA256withRSA");
            signature.initVerify(this.getPublicKey());
        } catch (IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            e.printStackTrace();
            return false;
        }
        try {
            signature.update((addressSignature.getAddress() + addressSignature.getCurrency() + addressSignature.getPaymentTag()).getBytes());

            return signature.verify(Base64.getDecoder().decode(addressSignature.getSignature()));
        } catch (SignatureException e) {
            e.printStackTrace();
            return false;
        }
    }

    private PublicKey getPublicKey() throws IOException {
        String rawPubKey = IOUtils.toString(
                this.getClass().getResourceAsStream(
                        "/" + Config.getInstance().getInfra().name().toLowerCase() + "/address_public_key.pem"
                ),
                StandardCharsets.UTF_8
        );

        return JwtService.getInstance().getPublicKey(rawPubKey);
    }

    public static AddressSignatureValidator getInstance() {
        if (instance == null) {
            instance = new AddressSignatureValidator();
        }

        return instance;
    }
}
