package com.paycoiner.client;

import com.google.gson.Gson;
import com.paycoiner.client.domains.requests.CreateInvoice;
import com.paycoiner.client.domains.requests.CreatePayout;
import com.paycoiner.client.domains.requests.GetNewDepositAddress;
import com.paycoiner.client.domains.requests.ValidateAddress;
import com.paycoiner.client.domains.transfers.DepositAddress;
import com.paycoiner.client.domains.transfers.Invoice;
import com.paycoiner.client.domains.transfers.Payout;
import com.paycoiner.client.domains.transfers.PayoutEvent;
import com.paycoiner.client.exceptions.InvalidSignature;
import com.paycoiner.client.exceptions.requests.NotFound;
import com.paycoiner.client.exceptions.requests.RequestException;
import com.paycoiner.client.exceptions.requests.Unauthorized;
import com.paycoiner.client.exceptions.requests.Unprocessable;
import com.paycoiner.client.services.HmacService;
import com.paycoiner.client.services.JwtService;
import com.paycoiner.client.validators.AddressSignatureValidator;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.*;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

public class PaycoinerClient {
    CloseableHttpClient client;

    public PaycoinerClient() {
        this(HttpClients.createDefault());
    }

    public PaycoinerClient(CloseableHttpClient client) {
        this.client = client;
    }

    private <T> T sendGetRequest(String url, Map<String, String> query, Class<T> classOfT) throws IOException, RequestException {
        return this.sendGetRequest(url, query, classOfT, true);
    }

    private <T> T sendGetRequest(String url, Map<String, String> query, Class<T> classOfT, boolean withHmac) throws IOException, RequestException {
        if (query == null) {
            query = new HashMap<>();
        }
        if (withHmac && !query.containsKey("nonce")) {
            query.put("nonce", String.valueOf(System.currentTimeMillis()));
        }

        return this.sendRequest(HttpGet.METHOD_NAME, url, "", classOfT, query, withHmac, null);
    }

    private <T> T sendPostRequest(String url, Object body, Class<T> classOfT) throws IOException, RequestException {
        return this.sendPostRequest(url, body, classOfT, true);
    }

    private <T> T sendPostRequest(String url, Object body, Class<T> classOfT, boolean withHmac) throws IOException, RequestException {
        return this.sendRequest(HttpPost.METHOD_NAME, url, (new Gson()).toJson(body), classOfT, null, withHmac, null);
    }

    private <T> T sendRequest(String method, String url, String body, Class<T> classOfT, Map<String, String> query, boolean withHmac, String contentType) throws IOException, RequestException {
        URI uri;
        try {
            URIBuilder builder = new URIBuilder(url);
            if (query != null) {
                for (Map.Entry<String, String> entry : query.entrySet()) {
                    builder.addParameter(entry.getKey(), entry.getValue());
                }
            }
            uri = builder.build();
        } catch (URISyntaxException e) {
            throw new RequestException("", e);
        }

        HttpRequestBase request;
        if (method.equals(HttpPost.METHOD_NAME)) {
            request = new HttpPost(uri);
            ((HttpPost) request).setEntity(new StringEntity(body));
        } else if (method.equals(HttpGet.METHOD_NAME)) {
            request = new HttpGet(uri);
        } else {
            throw new RequestException("Unknown method: " + method);
        }
        this.appendHeadersToRequest(request, contentType);
        if (withHmac) {
            this.appendHmacToRequest(request);
        }

        CloseableHttpResponse response = client.execute(request);
        this.checkResponse(response);

        client.close();

        return (new Gson()).fromJson(EntityUtils.toString(response.getEntity()), classOfT);
    }

    private void appendHeadersToRequest(HttpRequestBase request, String contentType) {
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", contentType == null ? "application/json" : contentType);
        request.setHeader("X-Requested-With", "XMLHttpRequest");
    }

    private void appendHmacToRequest(HttpRequestBase request) {
        String payload = "";
        if (request instanceof HttpEntityEnclosingRequestBase) {
            try {
                payload = EntityUtils.toString(((HttpEntityEnclosingRequestBase) request).getEntity());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            String query = request.getURI().getQuery();
            payload = request.getURI().getPath() + (query == null || query.equals("") ? "" : "?" + query);
        }

        String hmac = HmacService.getInstance().generate(payload, Config.getInstance().getEndpointKey());
        if (hmac != null) {
            request.setHeader("X-Api-Key", hmac);
            request.setHeader("X-Customer-Id", Config.getInstance().getCustomerId());
        }
    }

    private void checkResponse(CloseableHttpResponse response) throws RequestException {
        if (response == null) {
            throw new RequestException("");
        }
        int statusCode = response.getStatusLine().getStatusCode();
        String responseBody = "";
        try {
            responseBody = EntityUtils.toString(response.getEntity());
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (statusCode == HttpStatus.SC_NOT_FOUND) {
            throw new NotFound(responseBody);
        }
        if (statusCode == HttpStatus.SC_UNAUTHORIZED) {
            throw new Unauthorized(responseBody);
        }
        if (statusCode == HttpStatus.SC_UNPROCESSABLE_ENTITY) {
            throw new Unprocessable(responseBody);
        }
        if (statusCode >= HttpStatus.SC_BAD_REQUEST) {
            throw new RequestException(responseBody);
        }
    }

    private HashMap<String, String> mapCurrencyAmount(@SuppressWarnings("rawtypes") HashMap response) {
        HashMap<String, String> result = new HashMap<>();
        for (Object el : response.entrySet()) {
            @SuppressWarnings("rawtypes") Map.Entry entry = (Map.Entry) el;
            result.put(entry.getKey().toString(), entry.getValue().toString());
        }

        return result;
    }

    public DepositAddress getNewDepositAddress(GetNewDepositAddress request) throws IOException, RequestException, InvalidSignature {
        DepositAddress address = this.sendPostRequest(
                Config.getInstance().getDepositAddressesUrl() + "/address",
                request,
                DepositAddress.class
        );
        AddressSignatureValidator.getInstance().check(address);

        return address;
    }

    public Invoice createInvoice(CreateInvoice request) throws IOException, RequestException, InvalidSignature {
        Invoice invoice = this.sendPostRequest(
                Config.getInstance().getPaymentsUrl() + "/invoices",
                request,
                Invoice.class
        );
        AddressSignatureValidator.getInstance().check(invoice);

        return invoice;
    }

    public Invoice getInvoice(String invoiceId) throws IOException, RequestException, InvalidSignature {
        Invoice invoice = this.sendGetRequest(
                Config.getInstance().getPaymentsUrl() + "/invoices/" + invoiceId,
                null,
                Invoice.class,
                false
        );
        AddressSignatureValidator.getInstance().check(invoice);

        return invoice;
    }

    /** Returns cryptocurrency as key, balance as value */
    public HashMap<String, String> getAvailableBalance() throws IOException, RequestException {
        return this.mapCurrencyAmount(
                this.sendGetRequest(
                    Config.getInstance().getPaymentsUrl() + "/balance",
                    null,
                    HashMap.class
            )
        );
    }

    public PayoutEvent createPayout(CreatePayout request) throws IOException, RequestException {
        return this.sendRequest(
                HttpPost.METHOD_NAME,
                Config.getInstance().getPayoutsUrl() + "/payouts",
                JwtService.getInstance().getJwt(request),
                PayoutEvent.class,
                null, false,
                "application/jwt"
        );
    }

    public Payout getPayout(String payoutId) throws IOException, RequestException {
        return this.sendGetRequest(
                Config.getInstance().getPayoutsUrl() + "/payouts/" + payoutId,
                null,
                Payout.class
        );
    }

    public HashMap<String, String> getBalanceOnPayout() throws IOException, RequestException {
        return this.mapCurrencyAmount(
            this.sendGetRequest(
                    Config.getInstance().getPayoutsUrl() + "/balance",
                    null,
                    HashMap.class
            )
        );
    }


    public HashMap<String, String> getEstimatedMinersFee() throws IOException, RequestException {
        return this.mapCurrencyAmount(
                this.sendGetRequest(
                        Config.getInstance().getPayoutsUrl() + "/currencies/estimated-fees",
                        null,
                        HashMap.class
                )
        );
    }

    public boolean validateAddress(ValidateAddress request) {
        try {
            return this.sendPostRequest(
                    Config.getInstance().getAddressValidatorUrl() + "/address/validate",
                    request,
                    HashMap.class,
                    false
            ).get("isValid").equals(true);
        } catch (IOException | RequestException e) {
            e.printStackTrace();
            return false;
        }
    }
}
