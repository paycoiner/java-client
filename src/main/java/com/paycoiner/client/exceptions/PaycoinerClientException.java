package com.paycoiner.client.exceptions;

public class PaycoinerClientException extends Exception {
    public PaycoinerClientException() {
    }

    public PaycoinerClientException(String message) {
        super(message);
    }

    public PaycoinerClientException(String message, Throwable cause) {
        super(message, cause);
    }

    public PaycoinerClientException(Throwable cause) {
        super(cause);
    }

    public PaycoinerClientException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
