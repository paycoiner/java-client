package com.paycoiner.client.exceptions.requests;

import com.paycoiner.client.exceptions.PaycoinerClientException;

public class RequestException extends PaycoinerClientException {
    protected String data;

    public RequestException(String data) {
        super(data);
        this.data = data;
    }

    public RequestException(String data, Throwable cause) {
        super(data, cause);
        this.data = data;
    }

    public String getData() {
        return data;
    }
}
