package com.paycoiner.client.exceptions.requests;

public class Unprocessable extends RequestException {
    public Unprocessable(String data) {
        super(data);
    }
}
