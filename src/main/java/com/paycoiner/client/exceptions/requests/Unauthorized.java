package com.paycoiner.client.exceptions.requests;

public class Unauthorized extends RequestException {
    public Unauthorized(String data) {
        super(data);
    }
}
