package com.paycoiner.client.exceptions.requests;

public class NotFound extends RequestException {
    public NotFound(String data) {
        super(data);
    }
}
