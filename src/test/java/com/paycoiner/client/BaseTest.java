package com.paycoiner.client;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public abstract class BaseTest {
    protected String getMockData(String path) throws IOException {
        return IOUtils.toString(this.getClass().getResourceAsStream(path), StandardCharsets.UTF_8);
    }
}
