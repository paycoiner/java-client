package com.paycoiner.client.interfaces;

import com.paycoiner.client.domains.transfers.Invoice;
import com.paycoiner.client.enums.InvoiceStatus;
import org.junit.Assert;

import java.util.Date;

public interface AssertInvoice {
    default void assertInvoiceResponse(Invoice invoice) {
        Assert.assertEquals("45511162-1802-11e9-ae97-fa163e025a6a", invoice.invoiceId);
        Assert.assertEquals("https://demo.paycoiner.com/invoice/view/?id=45511162-1802-11e9-ae97-fa163e025a6a", invoice.url);
        Assert.assertEquals("1547473364088", invoice.orderId);
        Assert.assertEquals(InvoiceStatus.NEW, invoice.status);
        Assert.assertEquals("2N9xESQREzgsbSyY78C2RoshV1qA4L3kY2o", invoice.address);
        Assert.assertEquals("BTC", invoice.currency);
        Assert.assertEquals("", invoice.paymentTag);
        Assert.assertNotNull(invoice.signature);
        Assert.assertEquals("BTC", invoice.baseCurrency);
        Assert.assertEquals("0.00002", invoice.baseAmount);
        Assert.assertEquals("0.00002", invoice.amount);
        Assert.assertEquals("0.00002", invoice.missing);
        Assert.assertEquals("0", invoice.paid);
        Assert.assertEquals("0", invoice.paidUnconfirmed);
        Assert.assertEquals("0", invoice.paidConfirmed);
        Assert.assertEquals("1", invoice.currencyRate);
        Assert.assertNull(invoice.email);

        Assert.assertEquals(invoice.expireAt, new Date(1547474264000L));
        Assert.assertEquals(invoice.createdAt, new Date(1547473364000L));
    }
}
