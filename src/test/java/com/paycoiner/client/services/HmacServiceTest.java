package com.paycoiner.client.services;

import org.junit.Assert;
import org.junit.Test;

public class HmacServiceTest {
    @Test
    public void testGenerateOk() {
        Assert.assertEquals(
                "6b014b1949397c544bf21f5b7f925c65d86ea87f66e3e26cda2ff3acb232f787",
                (new HmacService()).generate("{\"sampleData\":true}", "67890")
        );
    }

    @Test
    public void testValidateOk() {
        Assert.assertTrue(
                (new HmacService()).validate(
                        "6b014b1949397c544bf21f5b7f925c65d86ea87f66e3e26cda2ff3acb232f787",
                        "67890",
                        "{\"sampleData\":true}"
                )
        );
    }

    @Test
    public void testValidateFail() {
        Assert.assertFalse(
                (new HmacService()).validate(
                        "invalid-hash",
                        "67890",
                        "{\"sampleData\":true}"
                )
        );
    }
}
