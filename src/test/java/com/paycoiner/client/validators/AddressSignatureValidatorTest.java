package com.paycoiner.client.validators;

import com.paycoiner.client.interfaces.AddressSignature;
import org.junit.Assert;
import org.junit.Test;

public class AddressSignatureValidatorTest {
    @Test
    public void success() {
        boolean result = (new AddressSignatureValidator()).validate(new AddressSignature() {
            @Override
            public String getSignature() {
                return "o34CoumNxCmnYMVd8qu2LQfWLdY3IwXrnUzYa01yu1l+tnDrnpH1S/svFy9p/abkMksLpgyC9KvGdAIvw/gFQ3K1Z8/NRbKZWqJN2Tt4AuZfeDI0n3B/VCNkmLDIPhu0lPnlJEsb9KAvPNlCPcYuLdO0VKUag6cVz41lv7AQAaM=";
            }

            @Override
            public String getCurrency() {
                return "BTC";
            }

            @Override
            public String getAddress() {
                return "2N9xESQREzgsbSyY78C2RoshV1qA4L3kY2o";
            }

            @Override
            public String getPaymentTag() {
                return "";
            }
        });

        Assert.assertTrue(result);
    }

    @Test
    public void invalid() {
        boolean result = (new AddressSignatureValidator()).validate(new AddressSignature() {
            @Override
            public String getSignature() {
                return "o34CoumNxCmnYMVd8qu2LQfWLdY3IwXrnUzYa01yu1l+tnDrnpH1S/svFy9p/abkMksLpgyC9KvGdAIvw/gFQ3K1Z8/NRbKZWqJN2Tt4AuZfeDI0n3B/VCNkmLDIPhu0lPnlJEsb9KAvPNlCPcYuLdO0VKUag6cVz41lv7AQAaM=";
            }

            @Override
            public String getCurrency() {
                return "ETH";
            }

            @Override
            public String getAddress() {
                return "0xe8f2a6a552cb6bf9799d55e13a43bb8392da947c";
            }

            @Override
            public String getPaymentTag() {
                return "";
            }
        });

        Assert.assertFalse(result);
    }
}
