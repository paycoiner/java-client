package com.paycoiner.client.handlers;

import com.paycoiner.client.domains.transfers.Deposit;
import org.junit.Assert;

public class DepositsHandlerTest extends AHandler<Deposit> {
    @Override
    protected String getValidHmac() {
        return "c3f59c29d61eea046c59c26351f241a6984a56d26e92941c9162611609a6e152";
    }

    @Override
    protected String getName() {
        return "deposits";
    }

    @Override
    protected HmacHandler<Deposit> getHandler() {
        return new DepositsHandler();
    }

    @Override
    protected HmacHandler<Deposit> getHandlerWithKey(String key) {
        return new DepositsHandler(key);
    }

    @Override
    protected void assertDomainObject(Deposit deposit) {
        Assert.assertEquals("d17e5e28-23b1-11e9-a439-507b9dfbcb8f", deposit.depositId);
        Assert.assertEquals("41eb8150-5227-11ea-97df-0242ac150005", deposit.customerId);
        Assert.assertEquals("51b78168d94ec307e2855697209275d477e05d8647caf29cb9e38fb6a4661145", deposit.txId);
        Assert.assertEquals("13iTN6jMCjGazw26mZNY8MtfAj3D8rQyDH", deposit.address);
        Assert.assertEquals("0.01002645", deposit.amount);
        Assert.assertEquals("BTC", deposit.currency);
        Assert.assertEquals(13, deposit.confirmations.intValue());
        Assert.assertFalse(deposit.finalConfirmed);
        Assert.assertFalse(deposit.rejected);
    }
}
