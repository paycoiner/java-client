package com.paycoiner.client.handlers;

import com.paycoiner.client.domains.transfers.PayoutEvent;
import org.junit.Assert;

public class PayoutsHandlerTest extends AHandler<PayoutEvent> {
    @Override
    protected String getValidHmac() {
        return "43712fe5924c4bf68565a7a5e76e639eaebe8c8d2c2b52c9668e8ba729146452";
    }

    @Override
    protected String getName() {
        return "payouts";
    }

    @Override
    protected HmacHandler<PayoutEvent> getHandler() {
        return new PayoutsHandler();
    }

    @Override
    protected HmacHandler<PayoutEvent> getHandlerWithKey(String key) {
        return new PayoutsHandler(key);
    }

    @Override
    protected void assertDomainObject(PayoutEvent payout) {
        Assert.assertEquals("90ea63da-ce46-11e9-b829-fa163ee1a16f", payout.customerId);
        Assert.assertEquals("1", payout.orderId);
        Assert.assertEquals("00260648-37f1-11e8-bbb9-507b9dfbcb8f", payout.payoutId);
        Assert.assertEquals("0.00012345", payout.fee);
        Assert.assertNull(payout.txId);
    }
}
