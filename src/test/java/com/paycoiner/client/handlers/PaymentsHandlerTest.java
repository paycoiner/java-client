package com.paycoiner.client.handlers;

import com.paycoiner.client.domains.transfers.Invoice;
import com.paycoiner.client.exceptions.InvalidSignature;
import com.paycoiner.client.exceptions.PaycoinerClientException;
import com.paycoiner.client.interfaces.AssertInvoice;
import org.junit.Test;

import java.io.IOException;

public class PaymentsHandlerTest extends AHandler<Invoice> implements AssertInvoice {
    @Override
    protected String getValidHmac() {
        return "e0c94d154c4f3cad4931fa14aeeaa244a4a93a83dc97730ed4a655370160193a";
    }

    @Override
    protected String getName() {
        return "payments";
    }

    @Override
    protected HmacHandler<Invoice> getHandler() {
        return new InvoicesHandler();
    }

    @Override
    protected HmacHandler<Invoice> getHandlerWithKey(String key) {
        return new InvoicesHandler(key);
    }

    @Test(expected = InvalidSignature.class)
    public void handleInvalidSignature() throws IOException, PaycoinerClientException {
        this.getHandlerWithKey().handle(
                this.getMockData("webhook-invalid-signature.json"),
                "d8144214fa3c40425eb9df3156f9506548596bce1fef57e60eab50b6d325101d"
        );
    }

    @Override
    protected void assertDomainObject(Invoice invoice) {
        this.assertInvoiceResponse(invoice);
    }
}
