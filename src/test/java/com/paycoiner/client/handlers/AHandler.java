package com.paycoiner.client.handlers;

import com.paycoiner.client.exceptions.InvalidHash;
import com.paycoiner.client.exceptions.PaycoinerClientException;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;

import static org.mockito.Mockito.when;

abstract class AHandler<T> {
    @Mock
    HttpServletRequest request;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    protected abstract String getValidHmac();

    protected abstract String getName();

    protected abstract HmacHandler<T> getHandler();

    protected abstract HmacHandler<T> getHandlerWithKey(String key);

    protected HmacHandler<T> getHandlerWithKey() {
        return this.getHandlerWithKey(this.getWebhookKey());
    }

    protected String getWebhookKey() {
        return "fiaegisrejgeoiesjeo";
    }

    protected String getMockData() throws IOException {
        return this.getMockData(null);
    }

    protected String getMockData(String filename) throws IOException {
        return IOUtils.toString(
                this.getClass().getResourceAsStream(
                        "/" + this.getName() + "/" + (filename == null ? "webhook-success.json" : filename)
                ),
                StandardCharsets.UTF_8
        );
    }

    protected BufferedReader getMockBufferedReader() throws IOException {
        return new BufferedReader(new StringReader(this.getMockData()));
    }

    @Test
    public void rawHandleOk() throws IOException, PaycoinerClientException {
        this.assertDomainObject(this.getHandlerWithKey().handle(this.getMockData(), this.getValidHmac()));
    }

    @Test
    public void requestHandleOk() throws IOException, PaycoinerClientException {
        when(request.getReader()).thenReturn(
                this.getMockBufferedReader()
        );
        when(request.getHeader("HMAC")).thenReturn(this.getValidHmac());
        this.assertDomainObject(this.getHandlerWithKey().handle(request));
    }

    @Test
    public void rawEnvKeyHandleOk() throws IOException, PaycoinerClientException {
        this.assertDomainObject(this.getHandler().handle(this.getMockData(), this.getValidHmac()));
    }

    @Test(expected = InvalidHash.class)
    public void handleInvalidKey() throws IOException, PaycoinerClientException {
        this.getHandlerWithKey("invalid").handle(this.getMockData(), this.getValidHmac());
    }

    @Test(expected = InvalidHash.class)
    public void handleInvalidHmac() throws IOException, PaycoinerClientException {
        this.getHandler().handle(this.getMockData(), "invalid");
    }

    protected abstract void assertDomainObject(T obj);
}
