package com.paycoiner.client;

import com.google.gson.Gson;
import com.paycoiner.client.domains.requests.CreateInvoice;
import com.paycoiner.client.domains.requests.CreatePayout;
import com.paycoiner.client.domains.requests.GetNewDepositAddress;
import com.paycoiner.client.domains.transfers.DepositAddress;
import com.paycoiner.client.domains.transfers.Invoice;
import com.paycoiner.client.domains.transfers.Payout;
import com.paycoiner.client.domains.transfers.PayoutEvent;
import com.paycoiner.client.enums.PayoutStatus;
import com.paycoiner.client.exceptions.InvalidSignature;
import com.paycoiner.client.exceptions.requests.NotFound;
import com.paycoiner.client.exceptions.requests.RequestException;
import com.paycoiner.client.exceptions.requests.Unauthorized;
import com.paycoiner.client.exceptions.requests.Unprocessable;
import com.paycoiner.client.interfaces.AssertInvoice;
import com.paycoiner.client.services.HmacService;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicStatusLine;
import org.apache.http.util.EntityUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.when;


public class ClientTest extends BaseTest implements AssertInvoice {
    @Mock
    private CloseableHttpClient httpClient;

    @Mock
    private CloseableHttpResponse httpResponse;

    @Captor
    private ArgumentCaptor<HttpUriRequest> captor;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getNewDepositAddress() throws InvalidSignature, IOException, RequestException {
        when(httpClient.execute(captor.capture())).thenReturn(httpResponse);
        when(httpResponse.getStatusLine()).thenReturn(new BasicStatusLine(HttpVersion.HTTP_1_1, HttpStatus.SC_OK, ""));
        when(httpResponse.getEntity()).thenReturn(new StringEntity(this.getMockData("/deposits/address.json")));

        GetNewDepositAddress addressRequest = new GetNewDepositAddress("1547473364088", "BTC");
        addressRequest.nonce = null; // disable nonce to make HMAC the same
        PaycoinerClient client = new PaycoinerClient(httpClient);
        DepositAddress response = client.getNewDepositAddress(addressRequest);

        Assert.assertEquals("406898", response.orderId);
        Assert.assertEquals("BTC", response.currency);
        Assert.assertEquals("8f539f02-8ed3-11ea-ba20-fa163e6e36e0", response.id);
        Assert.assertEquals("2MvaQxzorZEfPBkC365p1Thmz2HYQEU1LV4", response.address);
        Assert.assertEquals(
                "S0LrGsP0K/14aS5/l9igGJ4STMpHMVqCWSjABeFKd/Nz2C66fq7gjVX0Ja1nN6eVcZBEs8Bkzphly8osscQGRKfSCfVp3FDQIjDjG9B3LA6+a0HkRdY4FMTd/LWRDdJnG+t+jz5qtCBohPksR/02Kb0f2POEQg8HjmxG73ViPpo=",
                response.signature
        );

        HttpUriRequest request = captor.getValue();

        Assert.assertEquals(
                "https://deposit-addresses.sandbox.paycoiner.com/api/v1/address",
                request.getURI().toString()
        );
        Assert.assertEquals("POST", request.getMethod());
        Assert.assertEquals("1ee5470b745b770c75274e0c53529145e997778a27db21d0f149b0325e2b57fe", request.getFirstHeader("X-Api-Key").getValue());
        Assert.assertEquals("90ea63da-ce46-11e9-b829-fa163ee1a16f", request.getFirstHeader("X-Customer-Id").getValue());
        Assert.assertEquals("{\"customerId\":\"90ea63da-ce46-11e9-b829-fa163ee1a16f\",\"orderId\":\"1547473364088\",\"currency\":\"BTC\"}", EntityUtils.toString(((HttpEntityEnclosingRequestBase) request).getEntity()));
    }

    @Test
    public void createInvoice() throws InvalidSignature, IOException, RequestException {
        when(httpClient.execute(captor.capture())).thenReturn(httpResponse);
        when(httpResponse.getStatusLine()).thenReturn(new BasicStatusLine(HttpVersion.HTTP_1_1, HttpStatus.SC_OK, ""));
        when(httpResponse.getEntity()).thenReturn(new StringEntity(this.getMockData("/payments/webhook-success.json")));

        CreateInvoice createInvoiceRequest = new CreateInvoice(
                "1547473364088",
                "BTC",
                "BTC",
                "0.00002"
        );
        createInvoiceRequest.nonce = null; // disable nonce to make HMAC the same
        PaycoinerClient client = new PaycoinerClient(httpClient);
        Invoice response = client.createInvoice(createInvoiceRequest);
        this.assertInvoiceResponse(response);

        HttpUriRequest request = captor.getValue();

        Assert.assertEquals(
                "https://payments.sandbox.paycoiner.com/api/v1/invoices",
                request.getURI().toString()
        );
        Assert.assertEquals("POST", request.getMethod());
        Assert.assertEquals("e1fe44215de2e1e724228668313317498dcbbbde6715907c61657560df431264", request.getFirstHeader("X-Api-Key").getValue());
        Assert.assertEquals("90ea63da-ce46-11e9-b829-fa163ee1a16f", request.getFirstHeader("X-Customer-Id").getValue());
        Assert.assertEquals((new Gson()).toJson(createInvoiceRequest), EntityUtils.toString(((HttpEntityEnclosingRequestBase) request).getEntity()));
    }

    @Test
    public void getInvoice() throws InvalidSignature, IOException, RequestException {
        when(httpClient.execute(captor.capture())).thenReturn(httpResponse);
        when(httpResponse.getStatusLine()).thenReturn(new BasicStatusLine(HttpVersion.HTTP_1_1, HttpStatus.SC_OK, ""));
        when(httpResponse.getEntity()).thenReturn(new StringEntity(this.getMockData("/payments/webhook-success.json")));

        PaycoinerClient client = new PaycoinerClient(httpClient);
        Invoice response = client.getInvoice("5511162-1802-11e9-ae97-fa163e025a6a");
        this.assertInvoiceResponse(response);

        HttpUriRequest request = captor.getValue();
        Assert.assertEquals(
                "https://payments.sandbox.paycoiner.com/api/v1/invoices/5511162-1802-11e9-ae97-fa163e025a6a",
                request.getURI().toString()
        );
        Assert.assertEquals("GET", request.getMethod());
        Assert.assertNull(request.getFirstHeader("X-Api-Key"));
        Assert.assertNull(request.getFirstHeader("X-Customer-Id"));
    }

    @Test
    public void getAvailableBalance() throws IOException, RequestException {
        when(httpClient.execute(captor.capture())).thenReturn(httpResponse);
        when(httpResponse.getStatusLine()).thenReturn(new BasicStatusLine(HttpVersion.HTTP_1_1, HttpStatus.SC_OK, ""));
        when(httpResponse.getEntity()).thenReturn(new StringEntity(this.getMockData("/payments/balance.json")));

        PaycoinerClient client = new PaycoinerClient(httpClient);
        HashMap<String, String> response = client.getAvailableBalance();

        Assert.assertEquals(3, response.size());
        for (Map.Entry<String, String> entry : response.entrySet()) {
            if (entry.getKey().equals("ETH")) {
                Assert.assertEquals("2.35", entry.getValue());
                continue;
            }
            if (entry.getKey().equals("BTC")) {
                Assert.assertEquals("0.01", entry.getValue());
                continue;
            }
            if (entry.getKey().equals("LTC")) {
                Assert.assertEquals("1.01", entry.getValue());
            }
        }

        HttpUriRequest request = captor.getValue();
        String nonce = request.getURI().toString().split("=")[1];
        Assert.assertEquals(
                "https://payments.sandbox.paycoiner.com/api/v1/balance?nonce=" + nonce,
                request.getURI().toString()
        );
        Assert.assertEquals("GET", request.getMethod());
        Assert.assertEquals(
                HmacService.getInstance().generate("/api/v1/balance?nonce=" + nonce, Config.getInstance().getEndpointKey()),
                request.getFirstHeader("X-Api-Key").getValue()
        );
        Assert.assertEquals("90ea63da-ce46-11e9-b829-fa163ee1a16f", request.getFirstHeader("X-Customer-Id").getValue());
    }

    @Test
    public void createPayout() throws IOException, RequestException {
        when(httpClient.execute(captor.capture())).thenReturn(httpResponse);
        when(httpResponse.getStatusLine()).thenReturn(new BasicStatusLine(HttpVersion.HTTP_1_1, HttpStatus.SC_OK, ""));
        when(httpResponse.getEntity()).thenReturn(new StringEntity(this.getMockData("/payouts/webhook-success.json")));

        CreatePayout createPayoutRequest = new CreatePayout(
                "1",
                "BTC",
                "address",
                "0.00002"
        );
        createPayoutRequest.nonce = null;
        PaycoinerClient client = new PaycoinerClient(httpClient);
        PayoutEvent response = client.createPayout(createPayoutRequest);

        Assert.assertEquals(createPayoutRequest.customerId, response.customerId);
        Assert.assertEquals(createPayoutRequest.orderId, response.orderId);
        Assert.assertEquals("00260648-37f1-11e8-bbb9-507b9dfbcb8f", response.payoutId);

        HttpUriRequest request = captor.getValue();

        Assert.assertEquals(
                "https://payouts.sandbox.paycoiner.com/api/v2/payouts",
                request.getURI().toString()
        );
        Assert.assertEquals("POST", request.getMethod());
        Assert.assertEquals(
                "eyJhbGciOiJSUzI1NiJ9.eyJhbW91bnQiOiIwLjAwMDAyIiwiYWRkcmVzcyI6ImFkZHJlc3MiLCJvcmRlcklkIjoiMSIsImN1c3RvbWVySWQiOiI5MGVhNjNkYS1jZTQ2LTExZTktYjgyOS1mYTE2M2VlMWExNmYiLCJjdXJyZW5jeSI6IkJUQyJ9.bS9pGhIqOeuphd3zLl-veitTeuljV--bwnnHigHiS8L3ZIZMHcHmRGvzUJ1hIDO-UKJUDBsuMH-rO7uITXxO6fUWFQI_VuomIv_6HKsaRbA3fdPLemxnCaMN_-Mm-nZX9ZQQocoTwxMkXoKSt7NlvsDJKCy5CztsSedbMMsLMhtnFo8gIOKbawaoDvJPSOpPSzNIYzc9qjP2NFGCi9CwD-C5Fi8-u8MNjWehmgLh99-U2fcPZJfLS2Y4M8045UVMnFLKoIGseTQtrOpGD1iclflqPxwoNf6jdFnh-LlnPVAktSEzSJiFeSWPU4udPQZUvSP-hPYEs1gWaIxu-_pPug",
                EntityUtils.toString(((HttpEntityEnclosingRequestBase) request).getEntity())
        );
    }

    @Test
    public void getPayout() throws IOException, RequestException {
        when(httpClient.execute(captor.capture())).thenReturn(httpResponse);
        when(httpResponse.getStatusLine()).thenReturn(new BasicStatusLine(HttpVersion.HTTP_1_1, HttpStatus.SC_OK, ""));
        when(httpResponse.getEntity()).thenReturn(new StringEntity(this.getMockData("/payouts/payout.json")));

        PaycoinerClient client = new PaycoinerClient(httpClient);
        Payout response = client.getPayout("00260648-37f1-11e8-bbb9-507b9dfbcb8f");

        Assert.assertEquals("90ea63da-ce46-11e9-b829-fa163ee1a16f", response.customerId);
        Assert.assertEquals("1", response.orderId);
        Assert.assertEquals("00260648-37f1-11e8-bbb9-507b9dfbcb8f", response.payoutId);
        Assert.assertEquals("BTC", response.currency);
        Assert.assertEquals("address", response.address);
        Assert.assertEquals("0.0002", response.amount);
        Assert.assertNull(response.paymentTag);
        Assert.assertEquals(PayoutStatus.NEW, response.status);

        HttpUriRequest request = captor.getValue();
        String nonce = request.getURI().toString().split("=")[1];
        Assert.assertEquals(
                "https://payouts.sandbox.paycoiner.com/api/v2/payouts/00260648-37f1-11e8-bbb9-507b9dfbcb8f?nonce=" + nonce,
                request.getURI().toString()
        );
        Assert.assertEquals("GET", request.getMethod());
        Assert.assertEquals(
                HmacService.getInstance().generate("/api/v2/payouts/00260648-37f1-11e8-bbb9-507b9dfbcb8f?nonce=" + nonce, Config.getInstance().getEndpointKey()),
                request.getFirstHeader("X-Api-Key").getValue()
        );
        Assert.assertEquals("90ea63da-ce46-11e9-b829-fa163ee1a16f", request.getFirstHeader("X-Customer-Id").getValue());
    }

    private void makeRequest(int status) throws IOException, InvalidSignature, RequestException {
        when(httpClient.execute(captor.capture())).thenReturn(httpResponse);
        when(httpResponse.getStatusLine()).thenReturn(new BasicStatusLine(HttpVersion.HTTP_1_1, status, ""));
        when(httpResponse.getEntity()).thenReturn(new StringEntity(""));

        GetNewDepositAddress addressRequest = new GetNewDepositAddress("1547473364088", "BTC");
        PaycoinerClient client = new PaycoinerClient(httpClient);
        client.getNewDepositAddress(addressRequest);
    }

    @Test(expected = NotFound.class)
    public void notFoundException() throws InvalidSignature, IOException, RequestException {
        this.makeRequest(HttpStatus.SC_NOT_FOUND);
    }

    @Test(expected = Unauthorized.class)
    public void unauthorizedException() throws InvalidSignature, IOException, RequestException {
        this.makeRequest(HttpStatus.SC_UNAUTHORIZED);
    }

    @Test(expected = Unprocessable.class)
    public void unprocessableException() throws InvalidSignature, IOException, RequestException {
        this.makeRequest(HttpStatus.SC_UNPROCESSABLE_ENTITY);
    }
}
